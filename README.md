# IoT Device Emulator #

Based on example script from [End User IoT dashboards](https://www.youtube.com/watch?v=KMsODExqeIw)
video description - `emulator.js`.

---

![Console output example](assets/img/device-emulator-console-output.png)

---

### List of supported protocols ###

* [MQTT](https://thingsboard.io/docs/reference/mqtt-api/)

---

### How do I get set up? ###

* Clone
  ```bash
    $ git clone https://bitbucket.org/voodoo-crew/iot-device-emulator
  ```

* Configuration
  Check file `src/config/config.json` for fine tuning

* Install Dependencies
  ```bash
    npm i
  ```

* Tests
* Deployment instructions

---

### Changelog ###

#### Init ####
 * [x] Config file added
 * [x] Small refactoring.
 * [x] README file created
 * [x] Use `npm start` to run emulator with default test configuration

---

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
