/*!
 * Project: thingsboard.io
 * File:    ./src/app.js
 * Author:  Baltrushaitis Tomas <tbaltrushaitis@gmail.com>
 * Created: 2017-12-29
 */

'use strict';

/**
 * DEPENDENCIES
 * @private
 */

const fs   = require('fs');
const path = require('path');
const util = require('util');

const _    = require('lodash');
const mqtt = require('mqtt');

// Load Chance
const Chance = require('chance');

// Instantiate Chance so it can be used
const ch = new Chance();

/*
 * CONFIGURATION
 */

let ME = {};

// Below should be moved to widget config
const minWeight = 1;
const maxWeight = 100;

const appPath   =   path.dirname(module.filename);
const modName   =   path.basename(module.filename, '.js');
const modPath   =   path.relative(appPath, path.dirname(module.filename));
const modsPath  =   path.join(appPath, 'modules', path.sep);
const dataPath  =   path.join(appPath, 'data', path.sep);
const confBase  =   path.join(appPath, 'config');
const Config    =   require(confBase);

const utin = util.inspect;
utin.defaultOptions = _.extend({}, Config.get('iopts'));

let apiHost     =   Config.get('api:host');
let accessToken =   Config.get('api:token');
let topicAttrs  =   Config.get('topics:attributes');
let topicTelem  =   Config.get('topics:telemetry');
let oAttrs  =  require(`${path.join(dataPath, 'device-attributes.json')}`);

//  Initialization of mqtt client using Thingsboard host and device access token
console.log(`[${(new Date()).toISOString()}] Trying to connect to: [${apiHost}] with username: [${accessToken}]`);
let ioClient = mqtt.connect(`mqtt://${apiHost}`, {username: accessToken});

//  Triggers when client is successfully connected to the Thingsboard server
ioClient.on('connect', function () {

  console.log(`[${(new Date()).toISOString()}] MQTT Client connected to [${apiHost}]`);
  ioClient.publish(topicAttrs, JSON.stringify(oAttrs));

  console.log(`[${(new Date()).toISOString()}] Start uploading coordinates and rssi value [once per N seconds]`);
  setInterval(pubTelemetry, Config.get('app:delay'));

});

//  Uploads telemetry data using 'v1/devices/me/telemetry' MQTT topic
function pubTelemetry () {
  let loData = genFlex();
  ioClient.publish(topicTelem, JSON.stringify(loData));
  console.log(`[${(new Date()).toISOString()}] Uploaded telemetry data: [${utin(loData)}]`);
}

// let coords = ch.coordinates();
function genFlex () {
  let crdLat = ch.latitude();
  let crdLng = ch.longitude();
  let mark = ch.natural({min: minWeight, max: maxWeight});
  let Plot = _.extend({}, {
                  lat:  crdLat
                , lng:  crdLng
                , rssi: mark
              });
  return Plot;
}

// Catches ctrl+c event
process.on('SIGINT', function () {
  console.log(`[${(new Date()).toISOString()}] Disconnecting from server ...`);
  ioClient.end();
  console.log(`[${(new Date()).toISOString()}] Now QUIT`);
  process.exit(2);
});

// Catches uncaught exceptions
process.on('uncaughtException', function (e) {
  console.log(`[${(new Date()).toISOString()}] UncaughtException [${e.message}]:
    {stack: ${e.stack ? utin(e) : 'N/A'}}`);
  process.exit(99);
});
